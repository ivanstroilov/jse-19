package ru.t1.stroilov.tm.api.repository;

import ru.t1.stroilov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    Task add(String userId, String name, String description);

    Task add(String userId, String name);

    List<Task> findAllByProjectID(String userId, String projectId);
}
