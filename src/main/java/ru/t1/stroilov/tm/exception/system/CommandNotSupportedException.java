package ru.t1.stroilov.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(final String message) {
        super("Error! Command \"" + message + "\" not supported...");
    }
}
