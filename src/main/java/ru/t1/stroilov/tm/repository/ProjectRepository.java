package ru.t1.stroilov.tm.repository;

import ru.t1.stroilov.tm.api.repository.IProjectRepository;
import ru.t1.stroilov.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public Project add(final String userId, final String name, final String description) {
        final Project project = new Project(name, description);
        project.setUserId(userId);
        return add(project);
    }

    @Override
    public Project add(final String userId, final String name) {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

}
