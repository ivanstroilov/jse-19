package ru.t1.stroilov.tm.repository;

import ru.t1.stroilov.tm.api.repository.IUserOwnedRepository;
import ru.t1.stroilov.tm.enumerated.Sort;
import ru.t1.stroilov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    protected Predicate<M> filterByUserId(final String userId) {
        return m -> userId.equals(m.getUserId());
    }

    @Override
    public void deleteAll(final String userId) {
        final List<M> models = findAll(userId);
        deleteAll(models);
    }

    @Override
    public boolean existsById(String userId, String id) {
        return findById(userId, id) != null;
    }

    @Override
    public List<M> findAll(String userId) {
        return findAll().stream().filter(filterByUserId(userId)).collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(String userId, Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public List<M> findAll(String userId, Sort sort) {
        if (userId == null) return null;
        if (sort == null) return findAll(userId);
        final Comparator<M> comparator = (Comparator<M>) sort.getComparator();
        if (comparator == null) return findAll(userId);
        return findAll(comparator);
    }

    @Override
    public M findById(String userId, String id) {
        if (userId == null || id == null) return null;
        return findAll().stream().filter(filterByUserId(userId)).filter(filterById(id)).findFirst().orElse(null);
    }

    @Override
    public M findByIndex(String userId, Integer index) {
        return findAll().stream().filter(filterByUserId(userId)).skip(index).findFirst().orElse(null);
    }

    @Override
    public int getSize(String userId) {
        return (int) findAll().stream().filter(filterByUserId(userId)).count();
    }

    @Override
    public M deleteById(String userId, String id) {
        if (userId == null) return null;
        final M model = findById(userId, id);
        if (model == null) return null;
        return delete(model);
    }

    @Override
    public M deleteByIndex(String userId, Integer index) {
        if (userId == null) return null;
        final M model = findByIndex(userId, index);
        if (model == null) return null;
        return delete(model);
    }

    @Override
    public M add(String userId, M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        models.put(model.getId(), model);
        return model;
    }

    @Override
    public M delete(String userId, M model) {
        if (userId == null) return null;
        if (model == null) return null;
        return deleteById(userId, model.getId());
    }
}
