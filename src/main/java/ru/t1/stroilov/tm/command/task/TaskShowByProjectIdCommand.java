package ru.t1.stroilov.tm.command.task;

import ru.t1.stroilov.tm.model.Task;
import ru.t1.stroilov.tm.util.TerminalUtil;

import java.util.List;

public class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    public final static String DESCRIPTION = "Show Task by Project ID.";

    public final static String NAME = "task-show-by-project-id\"";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("Enter Project id:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getTaskService().findAllByProjectID(getUserId(), projectId);
        showTaskList(tasks);
        System.out.println("[OK]");
    }
}
